# Danish translation of release-notes for Debian 8.
# Copyright (C) 2015 Free Software Foundation, Inc.
# Martin Bagge <brother@bsnet.se>, 2009, 2011.
# Ask Hjorth Larsen <asklarsen@gmail.com>, 2010.
# Joe Hansen <joedalton2@yahoo.dk>, 2012, 2013, 2015.
#
# Debian Reference, Debian Maintainers Guide osv. - kaldes de noget særligt på dansk?
# bug tracking system -> fejlsporingssystem?
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 8 moreinfo.po\n"
"POT-Creation-Date: 2019-07-06 10:16+0200\n"
"PO-Revision-Date: 2015-04-11 01:28+0100\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute 'lang' of: <chapter>
#: en/moreinfo.dbk:8
msgid "en"
msgstr "da"

#. type: Content of: <chapter><title>
#: en/moreinfo.dbk:9
msgid "More information on &debian;"
msgstr "Yderligere oplysninger om &debian;"

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:11
msgid "Further reading"
msgstr "Yderligere læsning"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:13
#, fuzzy
#| msgid ""
#| "Beyond these release notes and the installation guide, further "
#| "documentation on &debian; is available from the Debian Documentation "
#| "Project (DDP), whose goal is to create high-quality documentation for "
#| "Debian users and developers.  Available documentation includes the Debian "
#| "Reference, Debian New Maintainers Guide, the Debian FAQ, and many more.  "
#| "For full details of the existing resources see the <ulink url=\"&url-ddp;"
#| "\">Debian Documentation website</ulink> and the <ulink url=\"&url-wiki;"
#| "\">Debian Wiki website</ulink>."
msgid ""
"Beyond these release notes and the installation guide, further documentation "
"on Debian is available from the Debian Documentation Project (DDP), whose "
"goal is to create high-quality documentation for Debian users and "
"developers, such as the Debian Reference, Debian New Maintainers Guide, the "
"Debian FAQ, and many more.  For full details of the existing resources see "
"the <ulink url=\"&url-ddp;\">Debian Documentation website</ulink> and the "
"<ulink url=\"&url-wiki;\">Debian Wiki</ulink>."
msgstr ""
"Ud over disse udgivelsesnoter og installationsvejledningen, kan man finde "
"yderligere dokumentation til &debian; hos Debian Documentation Project "
"(DDP), hvis mål er at skabe dokumentation af høj kvalitet til Debians "
"brugere og udviklere. Blandt den tilgængelige dokumentation findes Debian "
"Reference, Debian New Maintainers Guide og Debian OSS samt meget mere. De "
"fuldstændige detaljer om eksisterende materiale kan findes på <ulink url="
"\"&url-ddp;\">Debians dokumentationsprojekts internetside</ulink> og <ulink "
"url=\"&url-wiki;\">Debians wiki</ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:23
msgid ""
"Documentation for individual packages is installed into <filename>/usr/share/"
"doc/<replaceable>package</replaceable></filename>.  This may include "
"copyright information, Debian specific details, and any upstream "
"documentation."
msgstr ""
"Dokumentationen for enkelte pakker installeres i <filename>/usr/share/doc/"
"<replaceable>pakke</replaceable></filename>. Dette kan omfatte oplysninger "
"om ophavsret, Debian-specifikke detaljer samt dokumentation fra programmets "
"ophavssted."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:31
msgid "Getting help"
msgstr "Få hjælp"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:33
#, fuzzy
#| msgid ""
#| "There are many sources of help, advice, and support for Debian users, but "
#| "these should only be considered if research into documentation of the "
#| "issue has exhausted all sources.  This section provides a short "
#| "introduction to these sources which may be helpful for new Debian users."
msgid ""
"There are many sources of help, advice, and support for Debian users, though "
"these should only be considered after researching the issue in available "
"documentation.  This section provides a short introduction to these sources "
"which may be helpful for new Debian users."
msgstr ""
"Der er mange steder at finde hjælp og rådgivning for Debianbrugere, men "
"disse bør først bruges, når man ikke kan finde svar på sit spørgsmål via de "
"andre dokumentationskilder. Dette afsnit giver en kort indledning til disse, "
"som kan være nyttige for nye Debianbrugere."

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:39
msgid "Mailing lists"
msgstr "E-post-lister"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:41
msgid ""
"The mailing lists of most interest to Debian users are the debian-user list "
"(English) and other debian-user-<replaceable>language</replaceable> lists "
"(for other languages).  For information on these lists and details of how to "
"subscribe see <ulink url=\"&url-debian-list-archives;\"></ulink>.  Please "
"check the archives for answers to your question prior to posting and also "
"adhere to standard list etiquette."
msgstr ""
"De mest interessante e-post-lister til Debianbrugere er den engelske liste "
"debian-user plus listerne debian-user-<replaceable>sprog</replaceable> for "
"andre sprog (den danske er <literal>debian-user-danish</literal>). "
"Oplysninger om disse lister og hvordan man abonnerer på dem kan findes på "
"<ulink url=\"&url-debian-list-archives;\"></ulink>. Se venligst i arkiverne "
"om dit spørgsmål allerede er besvaret, før du skriver, og følg i øvrigt "
"standard-etiketten for e-post-lister."

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:51
msgid "Internet Relay Chat"
msgstr "Internet Relay Chat"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:53
#, fuzzy
#| msgid ""
#| "Debian has an IRC channel dedicated to the support and aid of Debian "
#| "users, located on the OFTC IRC network.  To access the channel, point "
#| "your favorite IRC client at irc.debian.org and join <literal>#debian</"
#| "literal>."
msgid ""
"Debian has an IRC channel dedicated to support and aid for Debian users, "
"located on the OFTC IRC network.  To access the channel, point your favorite "
"IRC client at irc.debian.org and join <literal>#debian</literal>."
msgstr ""
"Debian har en IRC-kanal med det formål at hjælpe Debianbrugere. Kanalen kan "
"findes på IRC-netværket OFTC. Brug din foretrukne IRC-klient til at forbinde "
"til irc.debian.org og gå så ind på kanalen <literal>#debian</literal>."

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:58
msgid ""
"Please follow the channel guidelines, respecting other users fully.  The "
"guidelines are available at the <ulink url=\"&url-wiki;DebianIRC\">Debian "
"Wiki</ulink>."
msgstr ""
"Følg kanalens retningslinjer og udvis respekt for andre brugere. "
"Retningslinjerne kan findes på <ulink url=\"&url-wiki;DebianIRC\">Debians "
"wiki</ulink>."

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:63
msgid ""
"For more information on OFTC please visit the <ulink url=\"&url-irc-host;"
"\">website</ulink>."
msgstr ""
"Yderligere oplysninger om OFTC kan findes på <ulink url=\"&url-irc-host;"
"\">websiden</ulink>."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:71
msgid "Reporting bugs"
msgstr "Fejlrapportering"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:73
#, fuzzy
#| msgid ""
#| "We strive to make &debian; a high quality operating system; however that "
#| "does not mean that the packages we provide are totally free of bugs.  "
#| "Consistent with Debian's <quote>open development</quote> philosophy and "
#| "as a service to our users, we provide all the information on reported "
#| "bugs at our own Bug Tracking System (BTS).  The BTS is browseable at "
#| "<ulink url=\"&url-bts;\"></ulink>."
msgid ""
"We strive to make Debian a high-quality operating system; however that does "
"not mean that the packages we provide are totally free of bugs.  Consistent "
"with Debian's <quote>open development</quote> philosophy and as a service to "
"our users, we provide all the information on reported bugs at our own Bug "
"Tracking System (BTS).  The BTS can be browsed at <ulink url=\"&url-bts;\"></"
"ulink>."
msgstr ""
"Vi gør vores bedste for at gøre &debian; til et højkvalitetsstyresystem, men "
"dette betyder ikke at pakkerne altid er fri for fejl. Jævnfør Debians "
"filosofi om <quote>åben udvikling</quote>, og som en tjeneste for vores "
"brugere i almindelighed, kan alle oplysninger om de indrapporterede fejl "
"findes på vores fejlsporingssystem, BTS. BTS kan gennemses på <ulink url="
"\"&url-bts;\"></ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:81
msgid ""
"If you find a bug in the distribution or in packaged software that is part "
"of it, please report it so that it can be properly fixed for future "
"releases.  Reporting bugs requires a valid e-mail address.  We ask for this "
"so that we can trace bugs and developers can get in contact with submitters "
"should additional information be needed."
msgstr ""
"Hvis du finder en fejl i distributionen eller i de programpakker, som er en "
"del af den, så rapporter dem venligst så de kan blive rettet i fremtidige "
"udgivelser.  Fejlrapportering kræver en gyldig e-postadresse. Vi beder om "
"dette for, at vi kan spore fejlrapporterne, og så udviklerne kan kontakte "
"ophavspersonen hvis der kræves flere oplysninger."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:88
#, fuzzy
#| msgid ""
#| "You can submit a bug report using the program <command>reportbug</"
#| "command> or manually using e-mail.  You can read more about the Bug "
#| "Tracking System and how to use it by reading the reference documentation "
#| "(available at <filename>/usr/share/doc/debian</filename> if you have "
#| "<systemitem role=\"package\">doc-debian</systemitem> installed) or online "
#| "at the <ulink url=\"&url-bts;\">Bug Tracking System</ulink>."
msgid ""
"You can submit a bug report using the program <command>reportbug</command> "
"or manually using e-mail.  You can find out more about the Bug Tracking "
"System and how to use it by reading the reference documentation (available "
"at <filename>/usr/share/doc/debian</filename> if you have <systemitem role="
"\"package\">doc-debian</systemitem> installed) or online at the <ulink url="
"\"&url-bts;\">Bug Tracking System</ulink>."
msgstr ""
"Du kan indsende en fejlrapport ved hjælp af programmet <command>reportbug</"
"command> eller manuelt via e-post.  Du kan læse mere om fejlsporingssystemet "
"og hvordan det bruges ved at læse referencedokumentationen (som er "
"tilgængelig i <filename>/usr/share/doc/debian</filename>, hvis du har "
"installeret <systemitem role=\"package\">doc-debian</systemitem>) eller på "
"nettet via <ulink url=\"&url-bts;\">fejlsporingssystemet</ulink>."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:98
msgid "Contributing to Debian"
msgstr "Bidrag til Debian"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:100
#, fuzzy
#| msgid ""
#| "You do not need to be an expert to contribute to Debian.  By assisting "
#| "users with problems on the various user support <ulink url=\"&url-debian-"
#| "list-archives;\">lists</ulink> you are contributing to the community.  "
#| "Identifying (and also solving) problems related to the development of the "
#| "distribution by participating on the development <ulink url=\"&url-debian-"
#| "list-archives;\">lists</ulink> is also extremely helpful.  To maintain "
#| "Debian's high quality distribution, <ulink url=\"&url-bts;\">submit bugs</"
#| "ulink> and help developers track them down and fix them.  The tool "
#| "<systemitem role=\"package\">how-can-i-help</systemitem> helps you to "
#| "find suitable reported bugs to work on.  If you have a way with words "
#| "then you may want to contribute more actively by helping to write <ulink "
#| "url=\"&url-ddp-vcs-info;\">documentation</ulink> or <ulink url=\"&url-"
#| "debian-i18n;\">translate</ulink> existing documentation into your own "
#| "language."
msgid ""
"You do not need to be an expert to contribute to Debian.  By assisting users "
"with problems on the various user support <ulink url=\"&url-debian-list-"
"archives;\">lists</ulink> you are contributing to the community.  "
"Identifying (and also solving) problems related to the development of the "
"distribution by participating on the development <ulink url=\"&url-debian-"
"list-archives;\">lists</ulink> is also extremely helpful.  To maintain "
"Debian's high-quality distribution, <ulink url=\"&url-bts;\">submit bugs</"
"ulink> and help developers track them down and fix them.  The tool "
"<systemitem role=\"package\">how-can-i-help</systemitem> helps you to find "
"suitable reported bugs to work on.  If you have a way with words then you "
"may want to contribute more actively by helping to write <ulink url=\"&url-"
"ddp-vcs-info;\">documentation</ulink> or <ulink url=\"&url-debian-i18n;"
"\">translate</ulink> existing documentation into your own language."
msgstr ""
"Du behøver ikke være ekspert for at kunne bidrage til Debian. Ved at hjælpe "
"andre brugere med at løse problemer via de forskellige <ulink url=\"&url-"
"debian-list-archives;\">hjælpelister</ulink>, bidrager du til fællesskabet. "
"Fejlfinding (og også rettelse af) af problemet relateret til distributionens "
"udvikling via deltagelse på <ulink url=\"&url-debian-list-archives;"
"\">udviklerlisterne</ulink> er også særdeles nyttigt. For at hjælpe til med "
"at vedligeholde Debiandistributionens kvalitet, kan du <ulink url=\"&url-bts;"
"\">indsende fejlrapporter</ulink> og hjælpe udvikere med at finde og fikse "
"dem. Værktøjet <systemitem role=\"package\">how-can-i-help</systemitem> "
"hjælper dig med at finde egnede rapporter at arbejde på. Hvis du er god med "
"ord, kan du også bidrage aktivt ved at skrive <ulink url=\"&url-ddp-vcs-info;"
"\">dokumentation</ulink> eller <ulink url=\"&url-debian-i18n;\">oversætte</"
"ulink> eksisterende dokumentation til dit eget sprog."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:117
msgid ""
"If you can dedicate more time, you could manage a piece of the Free Software "
"collection within Debian.  Especially helpful is if people adopt or maintain "
"items that people have requested for inclusion within Debian.  The <ulink "
"url=\"&url-wnpp;\">Work Needing and Prospective Packages database</ulink> "
"details this information.  If you have an interest in specific groups then "
"you may find enjoyment in contributing to some of Debian's <ulink url=\"&url-"
"debian-projects;\">subprojects</ulink> which include ports to particular "
"architectures and <ulink url=\"&url-debian-blends;\">Debian Pure Blends</"
"ulink> for specific user groups, among many others."
msgstr ""
"Hvis du kan afsætte mere tid, kan du håndtere et stykke af Debians fri "
"softwaresamling. Det er især en hjælp hvis folk tager ansvaret for eller "
"vedligeholder ting, hvis inklusion i Debian forespørges af andre. Databasen "
"<ulink url=\"&url-wnpp;\">Work Needing and Prospective Packages</ulink> "
"indeholder denne type oplysninger. Hvis du er interesseret i specifikke "
"grupper, vil du måske finde det underholdende at bidrage til nogle af "
"Debians <ulink url=\"&url-debian-projects;\">underprojekter</ulink>, "
"inklusive portering til bestemte arkitekturer og <ulink url=\"&url-debian-"
"blends;\">Debian Pure Blends</ulink> for specifikke brugergrupper, blandt "
"mange andre."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:128
msgid ""
"In any case, if you are working in the free software community in any way, "
"as a user, programmer, writer, or translator you are already helping the "
"free software effort.  Contributing is rewarding and fun, and as well as "
"allowing you to meet new people it gives you that warm fuzzy feeling inside."
msgstr ""
"Under alle omstændigheder: Hvis du på nogen måde arbejder inden for den frie "
"programbevægelse, enten som bruger, programmør, dokumentationsforfatter "
"eller oversætter, hjælper du allerede de frie programmer. At bidrage er både "
"lønsomt og morsomt, lader dig møde nye mennesker, og giver dig en rar "
"fornemmelse indeni."
