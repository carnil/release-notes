#
# Simplified Chinese translation of Debian release notes
# Copyright (C) 2008 Debian Chinese project
# This file is distributed under the same license as the release
# notes.
#
# Authors:
# chenxianren <chenxianren@gmail.com>, 2008.
# Dongsheng Song <dongsheng.song@gmail.com>, 2008-2009
# LI Daobing <lidaobing@gmail.com>, 2008.
# Yangfl <mmyangfl@gmail.com>, 2017.
# Boyuan Yang <byang@debian.org>, 2019.
# Wenbin Lv <wenbin816@gmail.com>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 10.0\n"
"POT-Creation-Date: 2021-03-25 10:52-0400\n"
"PO-Revision-Date: 2019-07-11 21:03+0800\n"
"Last-Translator: Wenbin Lv <wenbin816@gmail.com>\n"
"Language-Team: Chinese (Simplified) <debian-l10n-chinese@lists.debian.org>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 2.2.3\n"

#. type: Attribute 'lang' of: <appendix>
#: en/old-stuff.dbk:8
msgid "en"
msgstr "zh_CN"

#. type: Content of: <appendix><title>
#: en/old-stuff.dbk:9
msgid "Managing your &oldreleasename; system before the upgrade"
msgstr "在升级前管理您的 &oldreleasename; 系统"

#. type: Content of: <appendix><para>
#: en/old-stuff.dbk:11
msgid ""
"This appendix contains information on how to make sure you can install or "
"upgrade &oldreleasename; packages before you upgrade to &releasename;.  This "
"should only be necessary in specific situations."
msgstr ""
"本附录包含在升级到 &releasename; 之前，如何确保您能够从 &oldreleasename; 安装"
"或升级软件包的相关信息。这应该只在特定的情况下需要。"

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:16
msgid "Upgrading your &oldreleasename; system"
msgstr "升级您的 &oldreleasename; 系统"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:18
msgid ""
"Basically this is no different from any other upgrade of &oldreleasename; "
"you've been doing.  The only difference is that you first need to make sure "
"your package list still contains references to &oldreleasename; as explained "
"in <xref linkend=\"old-sources\"/>."
msgstr ""
"基本上这和您之前已经做过的 &oldreleasename; 系统升级没有什么区别。唯一的区别"
"就是，您首先需要确定您的软件包列表仍旧引用 &oldreleasename; ，就像在<xref "
"linkend=\"old-sources\"/>中说明的那样。"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:24
msgid ""
"If you upgrade your system using a Debian mirror, it will automatically be "
"upgraded to the latest &oldreleasename; point release."
msgstr ""
"如果您从 Debian 镜像升级，将自动升级到最新的 &oldreleasename; 的小版本更新。"

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:30
msgid "Checking your APT source-list files"
msgstr "检查您的 APT source-list 文件"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:32
msgid ""
"If any of the lines in your APT source-list files (see <ulink url=\"&url-"
"man;/&releasename;/apt/sources.list.5.html\">sources.list(5)</ulink>)  "
"contain references to <quote><literal>stable</literal></quote>, this is "
"effectively pointing to &releasename; already. This might not be what you "
"want if you are not yet ready for the upgrade.  If you have already run "
"<command>apt update</command>, you can still get back without problems by "
"following the procedure below."
msgstr ""
"如果您的 APT source-list 文件（参见 <ulink url=\"&url-man;/&releasename;/apt/"
"sources.list.5.html\">sources.list(5)</ulink>）的任意一行包含了 "
"<quote><literal>stable</literal></quote> 这样的字符串，它在新的稳定版本发布后"
"实际上已经指向了 &releasename;。如果您还没准备好升级系统的话，这可能不是您想"
"要的结果。但如果您已经运行过了 <command>apt update</command> 命令的话，您仍然"
"可以采取下述步骤退回原状态而不产生额外问题。"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:40
msgid ""
"If you have also already installed packages from &releasename;, there "
"probably is not much point in installing packages from &oldreleasename; "
"anymore.  In that case you will have to decide for yourself whether you want "
"to continue or not.  It is possible to downgrade packages, but that is not "
"covered here."
msgstr ""
"如果您已经从 &releasename; 安装了软件包，就没有太大的必要再从 "
"&oldreleasename; 安装软件包了。在这种情况下，您必须决定是否继续或者终止。降级"
"软件包是可能的，但是不属于本文的讨论范围。"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:46
msgid ""
"As root, open the relevant APT source-list file (such as <filename>/etc/apt/"
"sources.list</filename>) with your favorite editor, and check all lines "
"beginning with <literal>deb http:</literal>, <literal>deb https:</literal>, "
"<literal>deb tor+http:</literal>, <literal>deb tor+https:</literal>, "
"<literal>URIs: http:</literal>, <literal>URIs: https:</literal>, "
"<literal>URIs: tor+http:</literal> or <literal>URIs: tor+https:</literal> "
"for a reference to <quote><literal>stable</literal></quote>.  If you find "
"any, change <literal>stable</literal> to <literal>&oldreleasename;</literal>."
msgstr ""
"以 root 权限身份用您喜欢的编辑器打开 APT source-list 文件（<filename>/etc/"
"apt/sources.list</filename>），检查所有以 <literal>deb http:</literal>、"
"<literal>deb https:</literal>、<literal>deb tor+http:</literal>、"
"<literal>deb tor+https:</literal>、<literal>URIs: http:</literal>、"
"<literal>URIs: https:</literal>、<literal>URIs: tor+http:</literal> 或 "
"<literal>URIs: tor+https:</literal> 开头，引用 <quote><literal>stable</"
"literal></quote> 的行。如果找到了，将 <literal>stable</literal> 改为 "
"<literal>&oldreleasename;</literal>。"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:57
msgid ""
"If you have any lines starting with <literal>deb file:</literal> or "
"<literal>URIs: file:</literal>, you will have to check for yourself if the "
"location they refer to contains a &oldreleasename; or &releasename; archive."
msgstr ""
"如果有些行以 <literal>deb file:</literal> 或 <literal>URIs: file:</literal> "
"开始，您应该自己检查该位置是否包含有 &oldreleasename; 或者 &releasename; 的档"
"案仓库。"

#. type: Content of: <appendix><section><important><para>
#: en/old-stuff.dbk:64
msgid ""
"Do not change any lines that begin with <literal>deb cdrom:</literal> or "
"<literal>URIs: cdrom:</literal>.  Doing so would invalidate the line and you "
"would have to run <command>apt-cdrom</command> again.  Do not be alarmed if "
"a <literal>cdrom:</literal> source line refers to <quote><literal>unstable</"
"literal></quote>.  Although confusing, this is normal."
msgstr ""
"不要修改任何以 <literal>deb cdrom:</literal> 或 <literal>URIs: cdrom:</"
"literal> 开始的行。如果这样做了的话，这些行就会失效，您将需要重新运行 "
"<command>apt-cdrom</command> 命令以启用从光盘更新的功能。即使在 "
"<literal>cdrom:</literal> 的行发现 <quote><literal>unstable</literal></"
"quote> 这样的字符串也不要感到奇怪。虽然令人困惑，但这是正常的。"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:73
msgid "If you've made any changes, save the file and execute"
msgstr "如果做了任何修改，保存文件后执行"

#. type: Content of: <appendix><section><screen>
#: en/old-stuff.dbk:76
#, no-wrap
msgid "# apt update\n"
msgstr "# apt update\n"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:79
msgid "to refresh the package list."
msgstr "以刷新软件包列表。"

#. type: Content of: <appendix><section><title>
#: en/old-stuff.dbk:84
msgid "Removing obsolete configuration files"
msgstr "删除过时的配置文件"

#. type: Content of: <appendix><section><para>
#: en/old-stuff.dbk:86
msgid ""
"Before upgrading your system to &releasename;, it is recommended to remove "
"old configuration files (such as <filename>*.dpkg-{new,old}</filename> files "
"under <filename>/etc</filename>) from the system."
msgstr ""
"在将系统升级到 &releasename; 之前，建议从系统中删除旧的配置文件（例如 "
"<filename>/etc</filename> 下的 <filename>*.dpkg-{new,old}</filename> 文件）。"

#~ msgid "Upgrade legacy locales to UTF-8"
#~ msgstr "将旧版区域设置升级为 UTF-8"

#~ msgid ""
#~ "Using a legacy non-UTF-8 locale has been unsupported by desktops and "
#~ "other mainstream software projects for a long time. Such locales should "
#~ "be upgraded by running <command>dpkg-reconfigure locales</command> and "
#~ "selecting a UTF-8 default. You should also ensure that users are not "
#~ "overriding the default to use a legacy locale in their environment."
#~ msgstr ""
#~ "使用陈旧的非 UTF-8 区域语言设置（locale）已有很长时间不受各类桌面环境和各"
#~ "个主流软件项目的支持。应当运行 <command>dpkg-reconfigure locales</"
#~ "command> 命令以更新并选择默认使用 UTF-8。您也应当确保用户不会覆盖默认设置"
#~ "并在他们的环境中使用陈旧的区域语言设置。"

#~ msgid ""
#~ "In the GNOME screensaver, using passwords with non-ASCII characters, "
#~ "pam_ldap support, or even the ability to unlock the screen may be "
#~ "unreliable when not using UTF-8.  The GNOME screenreader is affected by "
#~ "bug <ulink url=\"http://bugs.debian.org/599197\">#599197</ulink>.  The "
#~ "Nautilus file manager (and all glib-based programs, and likely all Qt-"
#~ "based programs too) assume that filenames are in UTF-8, while the shell "
#~ "assumes they are in the current locale's encoding. In daily use, non-"
#~ "ASCII filenames are just unusable in such setups.  Furthermore, the gnome-"
#~ "orca screen reader (which grants sight-impaired users access to the GNOME "
#~ "desktop environment) requires a UTF-8 locale since Squeeze; under a "
#~ "legacy characterset, it will be unable to read out window information for "
#~ "desktop elements such as Nautilus/GNOME Panel or the Alt-F1 menu."
#~ msgstr ""
#~ "在 GNOME 屏幕保护程序中，使用非 ASCII 字符的密码、使用 pam_ldap、甚至是在"
#~ "仅仅不使用 UTF-8 编码的情况下，解锁屏幕的功能都可能变得不可靠。GNOME 屏幕"
#~ "阅读器受到错误 <ulink url=\"http://bugs.debian.org/599197\">#599197</"
#~ "ulink> 的影响。Nautilus 文件管理器（以及所有基于 glib 的程序，有可能还有所"
#~ "有基于 Qt 的程序）都假定文件名为 UTF-8 编码，而 shell 则会假定它们使用当前"
#~ "区域设置的编码。在日常使用中，这种设置下非 ASCII 文件名基本就是不可用的。"
#~ "此外，自 Squeeze 以来 gnome-orca 屏幕阅读器（允许视障人士访问 GNOME 桌面环"
#~ "境）需要 UTF-8 区域设置;在遗留的字符集下，将无法读取 Nautilus/GNOME Panel "
#~ "或 Alt-F1 菜单等桌面元素的窗口信息。"

#~ msgid ""
#~ "If your system is localized and is using a locale that is not based on "
#~ "UTF-8 you should strongly consider switching your system over to using "
#~ "UTF-8 locales.  In the past, there have been bugs<placeholder type="
#~ "\"footnote\" id=\"0\"/> identified that manifest themselves only when "
#~ "using a non-UTF-8 locale. On the desktop, such legacy locales are "
#~ "supported through ugly hacks in the library internals, and we cannot "
#~ "decently provide support for users who still use them."
#~ msgstr ""
#~ "如果您的系统已本地化，并且正在使用不基于 UTF-8 的区域设置，则应强烈考虑将"
#~ "系统切换到使用 UTF-8 的区域设置上去。过去已有错误<placeholder type="
#~ "\"footnote\" id=\"0\"/>仅在使用非 UTF-8 语言环境时才会发生。在桌面上，这些"
#~ "传统的区域设置通过库内的肮脏手段来获得支持，我们无法向仍在使用它们的用户提"
#~ "供支持。"

#~ msgid ""
#~ "To configure your system's locale you can run <command>dpkg-reconfigure "
#~ "locales</command>. Ensure you select a UTF-8 locale when you are "
#~ "presented with the question asking which locale to use as a default in "
#~ "the system.  In addition, you should review the locale settings of your "
#~ "users and ensure that they do not have legacy locale definitions in their "
#~ "configuration environment."
#~ msgstr ""
#~ "要配置系统的区域设置，可以运行 <command>dpkg-reconfigure locales</"
#~ "command>。当您遇到询问系统默认区域设置的问题时，请确保选择 UTF-8 区域设"
#~ "置。此外，您应该查看用户的区域设置，并确保在他们的配置环境中没有旧的区域设"
#~ "置定义。"
